#include <iostream>
#include <cstdlib>

using std::cout;
using std::cin;

int EuclideanGCD (int a , int b){
    if (b==0){
        return a;
    }else
    {
        a = a%b;
        return EuclideanGCD(b,a);
    }
}

long long least_common_divisor(int a, int b){
    int aux = EuclideanGCD (a,b);
    //cout << aux<< "\n";
    a = a/aux;
    //cout<<a<<"\n"<<b<<"\n";
    long long result = (long long)a * b;
    //cout << result << "\n";
    return result;
}

int main(){
    int a, b;

    std::cin>>a;
    std::cin>>b;
    cout<<least_common_divisor(a,b)<<"\n";
    //cout<<NaiveGCD(a,b)<<"\n";
}