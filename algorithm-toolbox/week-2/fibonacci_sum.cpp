//solucion bruta uwu
#include <iostream>
#include <cassert>

int fibonacci_sum(long long n){
    int fibo[n+1];
    fibo[0] = 0;
    fibo[1] = 1;

    int sum=1;
    for(int i = 2; i<n+1;++i){
        fibo[i] = (fibo[i-1] ) + (fibo[i-2]);
        //fibo[i] = fibo[i]%10;
        sum = sum + fibo[i];
        //sum = sum%10;
    }
    
    if(n == 0){
        return 0;
    }else{
        if (n==1)
        {
            return 1;
        }
    }
    
    return sum ;
}

int main() {
    long long  n ;
    std::cin >> n;

    for (int i = 0; i<n+1; ++i){
        std::cout << fibonacci_sum(i) <<  " ";
    }
    std::cout<<"\n";
    return 0;
}