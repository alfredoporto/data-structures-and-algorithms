#include <iostream>

int fibonacci_last_digit(int n) {
    int fibo[n];
    fibo[0] = 1;
    fibo[1] = 1;
    for(int i = 2; i<n;++i){
        fibo[i] = ((fibo[i-1])%10 + (fibo[i-2])%10)%10;
    }

    return  fibo[ n-1];
}

int main() {
    int n = 0;
    std::cin >> n;

    std::cout << fibonacci_last_digit(n) << '\n';
    return 0;
}