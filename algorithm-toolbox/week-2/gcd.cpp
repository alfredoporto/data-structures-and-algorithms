#include <iostream>
#include <cstdlib>

using std::cout;
using std::cin;

int NaiveGCD ( int a, int b){
    int best = 1;
    int n = a  > b ? b : a;
   
    for ( int i = 1; i <= n ; i++) {
        if ((a%i == 0) && (b%i == 0))
            best = i;
    }
    return best;
}
int EuclideanGCD (int a , int b){
    if (b==0){
        return a;
    }else
    {
        a = a%b;
        return EuclideanGCD(b,a);
    }
    
}

int main(){
    int a, b;

    std::cin>>a;
    std::cin>>b;
    cout<<EuclideanGCD(a,b)<<"\n";
    //cout<<NaiveGCD(a,b)<<"\n";
}

