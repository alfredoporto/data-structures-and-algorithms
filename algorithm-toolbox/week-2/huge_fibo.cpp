#include <iostream>
#include<cmath>

int fibonacci_fast(int n) {
    int fibo[n+1];
    fibo[0] = 1;
    fibo[1] = 1;
    fibo[2] = 1;
    for(int i = 3; i<n+1;++i){
        fibo[i] = fibo[i-1] + fibo[i-2];
    }

    return  fibo[ n];
}

int huge_fibo(int n, int m){
    int module = fibonacci_fast(2*m);
    std::cout<<module<<"\n";
    int aux = n%module;
    std::cout<<aux<<"\n";
    aux = fibonacci_fast(aux);
     std::cout<<aux<<"\n";
    aux = aux % m;
    return aux;
   //return fibonacci_fast(aux)%m;
}

int main() {
    int n ,m;
    std::cin >> n;
    std::cin >> m;
    //std::cout << fibonacci_naive(n) << '\n';
    //test_solution();
    std::cout << huge_fibo(n, m) << '\n';
    return 0;
}