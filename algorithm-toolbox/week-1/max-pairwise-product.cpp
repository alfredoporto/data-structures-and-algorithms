#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using std::cout;

long long MaxPairWiseProductFast (const std::vector <int>& numbers){
	int n = numbers.size();
	int max_index1  = -1;
	for(int i = 0; i<n;++i){
		if ((max_index1 == -1) || (numbers[i] > numbers[max_index1]))
			max_index1 = i;

	}

	int max_index2 = -1;
	for (int j = 0; j < n; ++j)
		if((j != max_index1) && ((max_index2 == -1) || (numbers[j] > numbers[max_index2])))
			max_index2 = j;
	//std::cout<<max_index1<<" "<<max_index2<<"\n";
	return((long long)(numbers[max_index1])) * numbers[max_index2];
}

long long MaxPairWiseProductSlow(const std::vector<int>& numbers) {
    long long r = 0;
    int n = numbers.size();

    for (int first = 0; first < n; ++first) {
        for (int second = first + 1; second < n; ++second) {
            if (((long long)numbers[first])*numbers[second] > r)
			{
				/* code */r = ((long long)(numbers[first]))*numbers[second];
			}
        }
    }

    return r;
}

int main(){
	/*while(true){
		int n = rand() %1000 +2;
		std::cout<<n<<"\n";
		std::vector<int> a;
		for (int i = 0; i<n; ++i){
			a.push_back(rand()%10000);
		}
		for(int i=0; i<n; ++i){
			std::cout<<a[i]<< " ";
		}
		std::cout<<"\n";
		long long res1 = MaxPairWiseProductSlow(a);
		long long res2 = MaxPairWiseProductFast(a);
		if (res1 != res2){
			std::cout<<"Wrong Answer: "<<res1<< " "<<res2<<"\n";
			break;
		}else{
			std::cout<<"OK\n";	
		}
	}
	*/
	int n;
	std::cin>>n;
	std::vector<int> numbers(n);
	for (int i = 0; i<n; i++){
		std::cin>>numbers[i];
	}
	std::cout << MaxPairWiseProductFast(numbers) << "\n";
	
		
	return 0;
}

