#include <iostream>

int get_change(int m) {
    int res = 0;
    while (m){
        if(m/10 > 0){
            res += m/10;
            m %= 10;
        }else  if(m/5 > 0){
            res += m/5;
            m %=  5;
        }else
        {
            res += 1;
            m -=1; 
        }
    }
  return res;
}

int main() {
  int m;
  std::cin >> m;
  std::cout << get_change(m) << '\n';
}