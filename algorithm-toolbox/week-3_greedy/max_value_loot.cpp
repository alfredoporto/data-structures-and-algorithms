#include <iostream>
#include <algorithm>

using namespace std;

struct Item
{
    int value;
    int weight;
    //Item(int value, int weight) : value(value), weight(weight) 
    //{} 
};

bool compare(struct Item a, struct Item b){
    double p1 = (double)a.value/a.weight;
    double p2 = (double)b.value/b.weight;
    return p1 > p2;
}


double get_optimal_value(int capacity, struct Item items[], int n) {
  double value = 0.0;
  int currentWeightPacked = 0;

  sort(items, items + n, compare);
  for (int i=0; i<n;i++){
      if(currentWeightPacked + items[i].weight <= capacity){
          currentWeightPacked += items[i].weight;
          value += items[i].value;
      }
      else{
          int remaining = capacity - currentWeightPacked;
          double  aux = (double) remaining / items[i].weight;
          value += items[i].value * aux; 
          break;
      }
  }
  return value;
}

int main() {
  int n;
  int capacity;
 
  std::cin >> n >> capacity;
  struct Item items[n];
  for (int i=0;i<n;++i){
        std::cin>>items[i].value>>items[i].weight;
        //std::cout<<items[i].value<<items[i].weight;
  }

  double optimal_value = get_optimal_value(capacity, items, n);

  std::cout.precision(10);
  std::cout << optimal_value<< std::endl;
  return 0;
}