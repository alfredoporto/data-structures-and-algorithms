#include <iostream>

using namespace std;

int compute_min_refills(int dist, int tank, int stops[] , int n) {
    int  min_refills = 0;
    int distance_aux = 0;
    for(int i = 0; i < n; ++i){
            if (stops[i]>tank + distance_aux){
                if(tank + stops[i-1]<stops[i]){
                    return -1;
                }else{
                    distance_aux = stops[i-1];
                    min_refills += 1;
                    i -= 1;
                }   
            }
        }
    return min_refills;
}


int main() {
    int d ; //destination
    int m;//tank capacity
    int n ;//# of gas stations
    cin >> d;
    cin >> m;
    cin >> n;
    n +=1;//to include the destination as a node
    int stops[n];
    stops[n-1]=d;
    for (int i = 0; i < n-1; ++i){
        cin >> stops[i];
    }
    cout << compute_min_refills(d, m, stops,n ) << "\n";
    return 0;
}
